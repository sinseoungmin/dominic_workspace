import lightgbm as lgb
import xgboost as xgb
import pandas as pd
import numpy as np
import multiprocessing
import pickle
import random
import time
import shap
import os
import gc


from tigger.util.hive_tools import hive_connection, load_from_hive, load_to_hive

from sklearn.metrics import accuracy_score, recall_score, precision_score, precision_recall_curve
from sklearn.metrics import f1_score, confusion_matrix, roc_auc_score, average_precision_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestClassifier

from collections import defaultdict
from inspect import signature

import matplotlib.pyplot as plt
import seaborn as sns
# %matplotlib inline

# import warnings
# warning.filterwarning('ignore')

# load data
df_dev = pd.read_pickle('../data/df_dev_01.pkl')
print('df_dev: ', df_dev.shape)

# split data
df_trn_val, df_test = train_test_split(df_dev, stratify=df_dev['label'], test_size=0.2, random_state=23)
print('df_trn: ', df_trn_val.shape)
print('df_test: ', df_test.shape)


# sampling
def sampling(df, label_col, label_ratio=0.2):
    print('label_ratio: ', label_ratio)
    df_tmp = df.copy()
    target_idx = list(df_tmp[df_tmp[label_col]==1].index)
    sample_cnt = round(1/label_ratio) * len(target_idx)
    print('sample_cnt: ', sample_cnt)
    random_idx = random.sample(list(df_tmp[df_tmp[label_col]==0].index), sample_cnt)
    sample_idx = random_idx + target_idx
    
    return df_tmp.loc[sample_idx,:]
# sampling
df_sample_trn_val = sampling(df_trn_val, 'label')
print('df_sample_trn_val: ', df_sample_trn_val.shape)


idx_col = 'svc_mgmt_num'
target_col = 'label'

feature_cols = [col for col in df_dev.columns if col not in ['svc_mgmt_num', 'label']]


sample_idx_dev = df_sample_trn_val[[idx_col]]
sample_x_dev = df_sample_trn_val[feature_cols]
sample_y_dev = df_sample_trn_val[[target_col]]

sample_x_trn, sample_x_val, sample_y_trn, sample_y_val = train_test_split(sample_x_dev, sample_y_dev, stratify=sample_y_dev, test_size=0.2, random_state=23)

print('sample_x_trn: ', sample_x_trn.shape)
print('sample_x_val: ', sample_x_val.shape)
print('sample_y_trn: ', sample_y_trn.shape)
print('sample_y_val: ', sample_y_val.shape)


sample_lgb_trn = lgb.Dataset(sample_x_trn, sample_y_trn, feature_name=feature_cols)
sample_lgb_val = lgb.Dataset(sample_x_val, sample_y_val, feature_name=feature_cols)

params = {
    'boosting_type': 'gbdt',
    'objective': 'binary',
    'metric': 'auc',
    'is_unbalance': True,
    'num_leaves': 31,
    'learning_rate': 0.05,
    'num_threads': 30,
}

clf = lgb.train(params,
                train_set=sample_lgb_trn,
                valid_sets=[sample_lgb_trn, sample_lgb_val],
                num_boost_round=3000,
                early_stopping_rounds=100,
                verbose_eval=50)


def evaluate(y_true, y_pred, cutoff=0.5):
    
    y_pred_class = [1 if c >= cutoff else 0 for c in y_pred]
    
    # result
    df_result = pd.DataFrame({'y_true': y_true, 
                              'y_pred': y_pred,
                              'y_pred_class': y_pred_class})
    
    # eval metrics
    df_metrics = pd.DataFrame({'acc': accuracy_score(y_true, y_pred_class),
                               'recall': recall_score(y_true, y_pred_class),
                               'precision': precision_score(y_true, y_pred_class),
                               'f1_score': f1_score(y_true, y_pred_class),
                               'auc': roc_auc_score(y_true, y_pred)}, index=[0])
    print(df_metrics)
        
    # confusion matrix
    df_confusion_matrix = pd.DataFrame(confusion_matrix(y_true, 
                                                        y_pred_class,
                                                        labels=[1,0]),
                                       index=['true: 1', 'true: 0'], 
                                       columns=['pred: 1', 'pred: 0'])
    print(df_confusion_matrix)
    
    return df_result, df_metrics, df_confusion_matrix

# valid evaluation
df_result, df_metrics, df_confusion_matrix = evaluate(sample_y_val.label.values, clf.predict(sample_x_val))
df_result.to_pickle('sample_lgbm_result.pkl')

# test evaluation
idx_tst = df_test[[idx_col]]
x_tst = df_test[feature_cols]
y_tst = df_test[[target_col]]

df_result, df_metrics, df_confusion_matrix = evaluate(y_tst.label.values, clf.predict(x_tst))
df_result.to_pickle('sample_lgbm_tst_result.pkl')

# save model
clf.save_model('sample_lgbm.txt')
