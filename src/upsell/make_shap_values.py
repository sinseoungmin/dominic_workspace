from sklearn.ensemble import RandomForestClassifier
import lightgbm as lgb
import xgboost as xgb

import pandas as pd
import numpy as np
import pickle
import time
import shap
import os
import gc

import matplotlib.pyplot as plt
import seaborn as sns
# %matplotlib inline

from sklearn.model_selection import train_test_split

import shap
shap.initjs()

from sm_utils import sampling, evaluate, save_model, load_model
from sm_utils import make_train_data,  make_test_data, df_to_md_table


DATA_PATH = '/home/dmig/work/upsell/data'
MODEL_PATH = '/home/dmig/work/upsell/model'
RESULT_PATH = '/home/dmig/work/upsell/result/shap_value'


# load data
print('load_data +++')
sample01 = pd.read_pickle(os.path.join(RESULT_PATH,'sample01.pkl'))
sample04 = pd.read_pickle(os.path.join(RESULT_PATH,'sample04.pkl'))
sample05 = pd.read_pickle(os.path.join(RESULT_PATH,'sample05.pkl'))
sample06 = pd.read_pickle(os.path.join(RESULT_PATH, 'sample06.pkl'))
print('sample01: ', sample01.shape)
print('sample04: ', sample04.shape)
print('sample05: ', sample05.shape)
print('sample06: ', sample06.shape)

total_feature = list(sample01.columns)
feature_cols = [c for c in total_feature if c not in ['svc_mgmt_num', 'label']]

# # 1% stratified sampling
# print('stratified sampling +++')
# _, sample03  = train_test_split(df_mart03, test_size=0.01, stratify=df_mart03['label'], random_state=23)
# _, sample05  = train_test_split(df_mart05, test_size=0.01, stratify=df_mart05['label'], random_state=23)
# _, sample06  = train_test_split(df_input06, test_size=0.01, random_state=23)
# print('sample03: ', sample03.shape)
# print('sample05: ', sample05.shape)
# print('sample06: ', sample06.shape)
# sample03.to_pickle(os.path.join(RESULT_PATH, 'sample03.pkl'))
# sample05.to_pickle(os.path.join(RESULT_PATH, 'sample05.pkl'))
# sample06.to_pickle(os.path.join(RESULT_PATH, 'sample06.pkl'))


# load model
print('load model +++')### model
model01 = load_model(os.path.join(MODEL_PATH, 'model_201901.pkl'))
model05 = load_model(os.path.join(MODEL_PATH, 'model_201905.pkl'))

# multi cpu
model01.reset_parameter({'num_threads':20})
model05.reset_parameter({'num_threads':20})


# shap_explainer[고정]
print('shap_explainer +++')
explainer01 = shap.TreeExplainer(model01)
explainer05 = shap.TreeExplainer(model05)

# make shap value
t1 = time.time()
sv_01_05 = explainer01.shap_values(sample05[feature_cols])
np.save(os.path.join(RESULT_PATH, 'sv_01_05'), sv_01_05)

t2 = time.time()
print('sv_01_05 elapsed time: ', t2-t1 )

sv_05_01 = explainer05.shap_values(sample01[feature_cols])
np.save(os.path.join(RESULT_PATH, 'sv_05_01'), sv_05_01)

t3 = time.time()
print('sv_05_01 elapsed time: ', t3-t2)


sv_05_05 = explainer05.shap_values(sample05[feature_cols])
np.save(os.path.join(RESULT_PATH, 'sv_05_05'), sv_05_05)

t4 = time.time()
print('sv_05_05 elapsed time: ', t4-t3)