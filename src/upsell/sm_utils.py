import lightgbm as lgb
import xgboost as xgb
import pandas as pd
import numpy as np
import multiprocessing
import pickle
import random
import time
import shap
import os
import gc

from tigger.util.hive_tools import hive_connection, load_from_hive, load_to_hive

from sklearn.metrics import accuracy_score, recall_score, precision_score, precision_recall_curve
from sklearn.metrics import f1_score, confusion_matrix, roc_auc_score, average_precision_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

from collections import defaultdict
from inspect import signature
from tabulate import tabulate

import matplotlib.pyplot as plt
import seaborn as sns
# %matplotlib inline


# PATH
RESULT_PATH = '/home/dmig/work/upsell/result'
DATA_PATH = '/home/dmig/work/upsell/data'
MODEL_PATH = '/home/dmig/work/upsell/model'

# meta
df_meta = pd.read_csv(os.path.join(DATA_PATH,'column_meta.csv'))


# logodds to probability
def logit2prob(logit):
    odds = np.exp(logit)
    prob = odds / (1+odds)
    return prob


# save df -> md table format ---------------------------------------------------
def df_to_md_table(df, filepath):
    print('save to: ', filepath)
    df_tmp = df.copy()
    first_col = list(df_tmp.columns)[0]
    with open(filepath, 'w') as f:
        print(tabulate(df_tmp.set_index(first_col), headers='keys', tablefmt='pipe'), file=f)
        

# show column distribusion between label
def show_col_dist_per_label(df, col, log_yn=False):
    df_tmp = df[[col, 'label']]
    
    if log_yn:
        df_tmp[col] = np.log1p(df_tmp[col])
        
    sns.set(rc={'figure.figsize': (12, 5)})
    sns.distplot(df_tmp[df_tmp.label==0]['col'], norm_hist=True, color='r', label='label: 0')
    sns.distplot(df_tmp[df_tmp.label==1]['col'], norm_hist=True, color='b', label='label: 1')
    
    plt.show()
        


# save / load model ---------------------------------------------------
def save_model(model, file_name):
    path = os.path.join(MODEL_PATH, file_name)
    print('save model to ', path)
    with open(path, 'wb') as f:
        pickle.dump(model, f, protocol=pickle.HIGHEST_PROTOCOL)

def load_model(file_name):
    path = os.path.join(MODEL_PATH, file_name)
    print('load model from ', file_name)
    with open(path, 'rb') as f:
        model = pickle.load(f)
    return model


# delete dataframe ---------------------------------------------------
def delete_df(df):
    del df
    gc.collect()


# sampling ---------------------------------------------------
def sampling(df, label_col, label_ratio=0.2):
    if label_ratio is None:
        print('label_ratio: None')
        return df
    
    print('label/sample: ', label_ratio)
    df_tmp = df.copy()
    target_idx = list(df_tmp[df_tmp[label_col]==1].index)
    sample_cnt = round(1/label_ratio) * len(target_idx)
    neg_sample_cnt = sample_cnt - len(target_idx)
    random_idx = random.sample(list(df_tmp[df_tmp[label_col]==0].index), neg_sample_cnt)
    sample_idx = random_idx + target_idx
    print('sample/total: ', sample_cnt/df_tmp.shape[0])
    return df_tmp.loc[sample_idx,:]


# make train data ---------------------------------------------------
def make_train_data(df, label_ratio=0.2, val_ratio=0.2):
    """
    make train data that can put in model directly with
        1. down sampling
        2. train_test_split
    Args:
        label_ratio(float): sampling된 데이터에서 label의 비율
        val_ratio(float): train_test_split에서 test_size
    Returns:
    {
        'trn': {
                'idx': trn_idx, 
                'x': trn_x, 
                'y': trn_y
            },
        'val': {
                'idx': val_idx, 
                'x': val_x, 
                'y': val_y
            },
        }
    """
    
    print('make_train_data +++')
    df_tmp = df.copy()

    # hard coding
    idx_col = 'svc_mgmt_num'
    target_col = 'label'
    feature_cols = [col for col in df_tmp.columns if col not in [idx_col, target_col]]
    
    # sampling
    df_sample = sampling(df_tmp, target_col, label_ratio)
    # train_test_saplit
    df_trn, df_val = train_test_split(df_sample, stratify = df_sample[target_col],
                                     test_size=val_ratio, random_state=23)
    
    result = {
        'trn': {
            'idx': df_trn[[idx_col]], 
            'x': df_trn[feature_cols], 
            'y': df_trn[[target_col]]
        },
        'val': {
            'idx': df_val[[idx_col]], 
            'x': df_val[feature_cols],
            'y': df_val[[target_col]]
        }
    }
    print('make_data_for_modeling---')
    
    return result

# make train data ---------------------------------------------------
def make_test_data(df):
    df_tmp = df.copy()
    
    idx_col = 'svc_mgmt_num'
    target_col = 'label'
    feature_cols = [col for col in df_tmp.columns if col not in [idx_col, target_col]]
    
    result = {
        'idx': df_tmp[[idx_col]],
        'x': df_tmp[feature_cols],
        'y': df_tmp[[target_col]]
    }
    return result



# evaluate ---------------------------------------------------
def evaluate(x, y, clf, clf_name, feature_name=None, cutoff=0.5, 
             num_threads=30, file_name=None):
    
    y_true = y
    
    if clf_name == 'lgb':
        y_pred = clf.predict(x, num_threads=num_threads)
    else:
        y_pred = clf.predict_proba(x)[:,1]
    
    y_pred_class = [1 if c >= cutoff else 0 for c in y_pred]
    
    # result ----------
    df_result = pd.DataFrame({'y_true': y_true, 
                              'y_pred': y_pred,
                              'y_pred_class': y_pred_class})
    
    # eval metrics ----------
    df_metrics = pd.DataFrame({'acc': accuracy_score(y_true, y_pred_class),
                               'recall': recall_score(y_true, y_pred_class),
                               'precision': precision_score(y_true, y_pred_class),
                               'f1_score': f1_score(y_true, y_pred_class),
                               'auc': roc_auc_score(y_true, y_pred)}, index=[0])
    print(df_metrics)
        
    # confusion matrix ----------
    df_confusion_matrix = pd.DataFrame(confusion_matrix(y_true, 
                                                        y_pred_class,
                                                        labels=[1,0]),
                                       index=['true: 1', 'true: 0'], 
                                       columns=['pred: 1', 'pred: 0'])
    print(df_confusion_matrix)
    
    # feature importance ----------
    if clf_name == 'lgb':
        feature_importance = clf.feature_importance()
    else:
        feature_importance = clf.feature_importances_
        
    if clf_name == 'xgb':
        feature_name = feature_name
    else:
        feature_name = list(x.columns)
        
    df_fea = pd.DataFrame({
        'feature': feature_name,
        'importance': feature_importance
    })

    df_kor_fea = pd.merge(df_fea, df_meta[['dimension', 'description']], 
                          how='left', left_on='feature', right_on='dimension')
    df_kor_fea = df_kor_fea[['feature', 'description', 'importance']]
#     print(df_kor_fea.sort_values('importance', ascending=False).head(20))
    
    # df_dict ----------
    df_dict = {
        'result': df_result,
        'metrics': df_metrics,
        'confusion_matrix': df_confusion_matrix,
        'feature_importance': df_fea
    }
    
    # prediction score distribution ----------
    sns.set(rc={'figure.figsize': (20,5)})
    plt.subplot(1,2,1)
    plt.title('total prediction score')
    sns.distplot(df_result.y_pred, norm_hist=True, label='true: 0')

    plt.subplot(1,2,2)
    plt.title('each label prediction score')
    sns.distplot(df_result[df_result.y_true==0].y_pred, norm_hist=True, color='r', label='true: 0')
    sns.distplot(df_result[df_result.y_true==1].y_pred, norm_hist=True, label='true: 1')
#     plt.show()

    
    # precision recall curve ----------
    sns.set(rc={'figure.figsize': (10,5)})

    precision, recall, _ = precision_recall_curve(df_result.y_true, df_result.y_pred)
    average_precision = average_precision_score(df_result.y_true, df_result.y_pred)

    step_kwargs = ({'step': 'post'} if 'step' in signature(plt.fill_between).parameters else {})

    plt.step(recall, precision, color='b', alpha=0.2, where='post')
    plt.fill_between(recall, precision, alpha=0.2, color='b', **step_kwargs)
    
    p_ratio = df_result.y_true.sum()/df_result.shape[0]
    plt.hlines(p_ratio, 0, 1)

    plt.xlabel('recall')
    plt.ylabel('precision')
    plt.xlim([0.0, 1.0])    
    plt.title('precision-recall curve: AP={0:0.2f}'.format(average_precision))
    plt.show()

    
    if file_name:
#         now = time.localtime()
#         t = clf_name + '_%02d%02d_%02d%02d%02d.pkl'%(now.tm_mon, now.tm_mday, 
#                                                      now.tm_hour, now.tm_min, now.tm_sec)
        path = os.path.join(RESULT_PATH, file_name)
        print('-'*50)
        print('result dictionary save to {}'.format(path))
        save_model(df_dict, path)
    
    return df_dict
