# package
from sklearn.ensemble import RandomForestClassifier
import lightgbm as lgb
import xgboost as xgb

import pandas as pd
import numpy as np
import pickle
import time
import shap
import os
import gc

import matplotlib.pyplot as plt
import seaborn as sns

from sm_utils import sampling, evaluate, show_results, save_model, load_model, make_train_data,  make_test_data


DATA_PATH = '/home/dmig/work/upsell/data/'
RESULT_PATH = '/home/dmig/work/upsell/result/training_preriod/'

# load model(training w/ 201901 sampllig data)
clf = load_model('/home/dmig/work/upsell/result/model_selection/lgb_0703.pkl')

def test_different_period(clf, test_ym):
    # load data
    print('load data')
    df_mart = pd.read_pickle(os.path.join(DATA_PATH, 'df_mart_{}.pkl'.format(test_ym)))
    print('df_mart: ', df_mart.shape)
    
    # make test data format
    print('make test data format')
    result = make_test_data(df_mart)
    idx = result['idx']
    x_test = result['x']
    y_test = result['y']
    feature_cols = list(x_test.columns)
    print('x_test: ', x_test.shape)
    print('y_test: ', y_test.shape)
    
    # evaluation
    print('evaluation')
    t1 = time.time()
    lgb_result_dict = evaluate(x_test, y_test.label.values, clf, 'lgb')
    print('elapsed time: ', time.time()-t1)
    
    
test_period_list = ['201902', '201903', '201904', '201905']

for p in test_period_list:
    print('period: ', p)
    test_different_period(clf, p)