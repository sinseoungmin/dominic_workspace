import lightgbm as lgb
import xgboost as xgb
import pandas as pd
import numpy as np
import shap
import pickle
import os
import gc

import multiprocessing
import random
import time

from tigger.util.hive_tools import hive_connection, load_from_hive, load_to_hive

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import auc, accuracy_score, recall_score, precision_score
from sklearn.metrics import f1_score, confusion_matrix, mean_squared_error, roc_auc_score
from sklearn.preprocessing import LabelEncoder
from collections import defaultdict
from bayes_opt import BayesianOptimization
from sm_utils import sampling, evaluate, show_results, save_model, load_model, make_train_data,  make_test_data


DATA_PATH = '/home/dmig/work/upsell/data/'
RESULT_PATH = '/home/dmig/work/upsell/result/training_preriod/'


# load data
print('load data')
df_mart = pd.read_pickle(os.path.join(DATA_PATH, 'df_mart_{}.pkl'.format('201905')))
print('df_mart: ', df_mart.shape)


# make train data
result = make_train_data(df_mart)

trn_idx = result['trn']['idx']
x_trn = result['trn']['x']\\\
y_trn = result['trn']['y']

val_idx = result['val']['idx']
x_val = result['val']['x']
y_val = result['val']['y']

feature_cols = list(x_trn.columns)

lgb_trn = lgb.Dataset(x_trn, y_trn, feature_name=feature_cols)
lgb_val = lgb.Dataset(x_val, y_val, feature_name=feature_cols)

def lgb_eval(num_leaves, lambda_l1, lambda_l2):
    params = {
        'boosting_type': 'gbdt',
        'objective': 'binary',
        'metric': 'auc',
        'is_unbalance': True,
        'learning_rate': 0.05,
        'num_threads': 40,
    }
    
    params['num_leaves'] = int(round(num_leaves))
    params['lambda_l1'] = lambda_l1
    params['lambda_l2'] = lambda_l2
    
    clf = lgb.train(params,
                    train_set=lgb_trn,
                    valid_sets=[lgb_trn, lgb_val],
                    num_boost_round=5000,
                    early_stopping_rounds=100,
                    verbose_eval=100)
    
    return clf.best_score['valid_1']['auc']


target_params = {'num_leaves': (8, 512),
                 'lambda_l1': (0, 100),
                 'lambda_l2': (0, 100)}



lgbBO = BayesianOptimization(lgb_eval, target_params)

lgbBO.maximize(init_points=10, n_iter=100)



from pandas.io.json import json_normalize
df_params = json_normalize(pd.DataFrame(lgbBO.res)['params'])
df_res = pd.concat([df_params, pd.DataFrame(lgbBO.res)['target']], axis=1)
df_res.to_pickle('bayes_opt_for_may_input.pkl')