from dateutil.relativedelta import relativedelta
from datetime import datetime
import pandas as pd
import numpy as np

from sklearn.metrics import accuracy_score, recall_score, precision_score
from sklearn.metrics import f1_score, roc_auc_score, average_precision_score

from tigger.util.hive_tools import hive_connection, load_from_hive, load_to_hive, send_query


# TODO: 관리를 어떻게 할지? 테이블?
RECO_TYPE_META = {
    'upsell': {
        'prod_id': 'RC00000001',
        'prod_nm': '요금제업셀가망'
    },
    'equip_chg': {
        'prod_id': 'RC00000002',
        'prod_nm': '기기변경가망'
    },
    'five_g': {
        'prod_id': 'RC00000003',
        'prod_nm': '5G가망'
    }
}


def load_to_comm_table(df, table_name, reco_type, partition_key_list):
    """
    dumbo에 upload 후, comm에 insert
    """

    print('load_to_comm_table +++')
    dumbo_table_name = '{}_{}'.format(table_name, reco_type)

    # load to dumbo table
    print('load to dumbo table +++')
    conn = hive_connection('dmig')
    load_to_hive(conn, df, "dumbo.{}".format(dumbo_table_name))

    # insert to comm table
    print('insert to comm table +++')
    insert_sql = """
            INSERT  OVERWRITE TABLE comm.{TABLE_NAME} PARTITION ({PARTITION_KEYS})
            SELECT  *
            FROM    dumbo.{DUMBO_TABLE_NAME}
        """.format(TABLE_NAME=table_name, DUMBO_TABLE_NAME=dumbo_table_name,
                   PARTITION_KEYS=', '.join(partition_key_list))
    conn = hive_connection('dmig')
    send_query(conn, insert_sql)

    # drop dumbo table
    print('drop dumbo table +++')
    conn = hive_connection('dmig')
    send_query(conn, 'drop table dumbo.{}'.format(dumbo_table_name))
    print('load_to_comm_table ---\n')


def evaluate_model(df):
    """
    evaluate model

    input:
        df_result['label', 'score', 'label']
    """
    print('evaluate_model +++')
    df_tmp = df.copy()
    y_true = df_tmp['label'].astype(int)
    y_score = df_tmp['score'].astype(float)
    y_pred = df_tmp['pred'].astype(int)

    # eval metrics ----------
    df_metrics = pd.DataFrame({'acc': np.round(accuracy_score(y_true, y_pred), 4),
                               'recall': np.round(recall_score(y_true, y_pred), 4),
                               # hive error 때문에 _1 붙임
                               'precision_1': np.round(precision_score(y_true, y_pred), 4),
                               'f1_score': np.round(f1_score(y_true, y_pred), 4),
                               'auc': np.round(roc_auc_score(y_true, y_score), 4),
                               'auprc': np.round(average_precision_score(y_true, y_score), 4)}, index=[0])
    print('evaluate_model ---\n')
    return df_metrics


class OfflineTest4Binary:
    """

    local에서 충분히 모델링 한 후,
    다른 모델들과의 비교를 위해서 hive로 필요한 데이터들을 upload


    input:
        - reco_type[string]:    추천 타입
        - test_ym[string]:      테스트 데이터의 년월
        - model[object]:        학습된 모델 객체
        - model_name[string]:   모델명
        - feature_cols[list]:   모델 학습에 사용된 feature column list

    upload to
        - comm.item_reco_test
        - comm.item_reco_test_post_binary_feature_importance
        - comm.item_reco_test_post_binary_stat
        - comm.item_reco_test_post_binary_score_dist

    """

    def __init__(self, reco_type, test_ym, model, model_name, feature_cols):
        print('init +++')
        self.reco_type = reco_type
        self.prod_id = RECO_TYPE_META[reco_type]['prod_id']
        self.prod_nm = RECO_TYPE_META[reco_type]['prod_nm']
        self.test_ym = test_ym
        self.model = model
        self.model_name = model_name
        self.feature_cols = feature_cols

        print('reco_type: ', self.reco_type)
        print('prod_id: ', self.prod_id)
        print('prod_nm: ', self.prod_nm)
        print('test_ym: ', self.test_ym)
        print('model_name: ', self.model_name)
        print('feature_cols: ', self.feature_cols)
        print('init ---\n')

    def submit_to_item_reco_test(self, df_test):
        """
        학습된 model의 offline test 결과를 comm.item_reco_test에 upload

        input:
            - df_test[dataframe]:   테스트에 사용할 데이터(reco_type 별로 분석가 모두가 동일)
        """
        print('submit_to_item_reco_test +++')
        # calculate score
        df_submit = df_test[['svc_mgmt_num']]
        df_submit['prod_id'] = self.prod_id
        df_submit['prod_nm'] = self.prod_nm
        df_submit['score'] = np.round(
            self.model.predict(df_test[self.feature_cols]), 4)
        df_submit['reco_type'] = self.reco_type
        df_submit['model'] = self.model_name
        df_submit['ym'] = self.test_ym
        # load to hive
        load_to_comm_table(df_submit, 'item_reco_test',
                           self.reco_type, ['reco_type', 'model', 'ym'])
        print('submit_to_item_reco_test ---\n')

    def insert_feature_importance_top20(self):
        """
        학습된 model에서 중요도가 높은 상위 20개의 feature 이름과 그 정도를 hive에 적재
        """
        print('load_feature_importance_top20 +++')
        if 'lightgbm' in str(self.model):
            importance = self.model.feature_importance()
        else:
            importance = self.model.feature_importances_
        df_fea_top20 = pd.DataFrame({'feature_name': self.feature_cols,
                                     'importance': importance}).sort_values('importance', ascending=False).iloc[:20, :]
        df_fea_top20['reco_type'] = self.reco_type
        df_fea_top20['model'] = self.model_name
        df_fea_top20['ym'] = self.test_ym
        df_fea_top20.reset_index(inplace=True)
        df_fea_top20.drop('index', axis=1, inplace=True)

        # load to hive
        load_to_comm_table(
            df_fea_top20, 'item_reco_test_post_binary_feature_importance', self.reco_type, ['model', 'ym'])
        print('load_feature_importance_top20 ---\n')

    def load_item_reco_test_with_label(self):
        """
        item_reco_test 값과  user_label_matrix 값을 join해서
        추후 연산에 사용하기 위한 형태로 가져온다
        """
        print('load_item_reco_test_with_label +++')
        label_table = 'user_label_matrix_for_{}_monthly'.format(self.reco_type)
        next_ym = datetime.strptime(
            self.test_ym, '%Y%m') + relativedelta(months=1)
        next_ym = next_ym.strftime('%Y%m')

        sql = """
        select  a.svc_mgmt_num
        ,       a.score
        ,       if (a.score >= 0.5 , 1, 0) as pred
        ,       case when b.label = 'Y' then 1
                    when b.label = 'N' then 0
                else 'ERROR' end as label
        ,       a.model
        from
        (
            select svc_mgmt_num, score, model
            from comm.item_reco_test
            where model = '{MODEL}'
            and ym = '{YM}'
        ) a
        left join(
            select svc_mgmt_num, label
            from comm.{LABEL_TABLE}
            where ym = '{NEXT_YM}'
        )b on a.svc_mgmt_num = b.svc_mgmt_num
        """.format(MODEL=self.model_name, YM=self.test_ym,
                   NEXT_YM=next_ym, LABEL_TABLE=label_table)

        conn = hive_connection('dmig')
        df_result = load_from_hive(conn, sql)
        print('df: ', df_result.shape, df_result.head())
        print('load_item_reco_test_with_label ---\n')

        return df_result

    # TODO: k가 None인 경우도 생각해야하나?
    def insert_model_stat(self, df_result, k=6000000):
        """
        model의 performance를 계산해서 hive에 적재
        전체 / 상위 k명 별 performance 계산
        """
        print('load_model_stat +++')
        print('calculate total performance')
        # total performance
        total_performance = evaluate_model(df_result)
        total_performance['note'] = 'total'
        total_performance['reco_type'] = self.reco_type
        total_performance['model'] = self.model_name
        total_performance['ym'] = self.test_ym

        print('calculate top k performance')
        print('k: ', k)
        # 상위 k 명 performance
        df_sorted = df_result.copy().sort_values('score', ascending=False)
        df_top_k = df_sorted.iloc[:k, :]
        df_no_top_k = df_sorted.iloc[k:, :]

        df_top_k['pred'] = 1
        df_no_top_k['pred'] = 0

        df_processed = pd.concat([df_top_k, df_no_top_k])
        top_k_performance = evaluate_model(df_processed)
        top_k_performance['note'] = 'top_k'
        top_k_performance['reco_type'] = self.reco_type
        top_k_performance['model'] = self.model_name
        top_k_performance['ym'] = self.test_ym

        df_submit = pd.concat([total_performance, top_k_performance])

        # load to hive
        load_to_comm_table(
            df_submit, 'item_reco_test_post_binary_stat', self.reco_type, ['model', 'ym'])

        print('load_model_stat ---\n')

    def insert_score_distribution(self, df_result):
        """
        model score 값을 20개로 binning해서 그에 속하는 회선의 개수를 count해 hive에 적재(label 별로)
        """
        print('load_score_distribution +++')
        df_tmp = df_result.copy()
        df_tmp['label'] = df_tmp['label'].astype(int)
        df_tmp['score'] = df_tmp['score'].astype(float)

        # label = 1
        df_pos = df_tmp[df_tmp.label == 1]
        df_pos['score_bin'] = pd.cut(df_pos['score'], bins=np.arange(
            0, 1.05, 0.05), labels=np.arange(0, 1, 0.05))
        df_pos_score = pd.DataFrame(
            df_pos.score_bin.value_counts().sort_index())
        df_pos_score.reset_index(inplace=True)
        df_pos_score.columns = ['score_bin', 'cnt']
        df_pos_score['score_bin'] = df_pos_score['score_bin'].astype(float)
        df_pos_score['ratio'] = df_pos_score['cnt'] / df_pos_score['cnt'].sum()
        df_pos_score['label'] = 1
        df_pos_score['reco_type'] = self.reco_type
        df_pos_score['model'] = self.model_name
        df_pos_score['ym'] = self.test_ym

        # label = 0
        df_neg = df_tmp[df_tmp.label == 0]
        df_neg['score_bin'] = pd.cut(df_neg['score'], bins=np.arange(
            0, 1.05, 0.05), labels=np.arange(0, 1, 0.05))
        df_neg_score = pd.DataFrame(
            df_neg.score_bin.value_counts().sort_index())
        df_neg_score.reset_index(inplace=True)
        df_neg_score.columns = ['score_bin', 'cnt']
        df_neg_score['score_bin'] = df_neg_score['score_bin'].astype(float)
        df_neg_score['ratio'] = df_neg_score['cnt'] / df_neg_score['cnt'].sum()
        df_neg_score['label'] = 0
        df_neg_score['reco_type'] = self.reco_type
        df_neg_score['model'] = self.model_name
        df_neg_score['ym'] = self.test_ym

        df_total = pd.concat([df_pos_score, df_neg_score])

        # load to hive
        load_to_comm_table(
            df_total, 'item_reco_test_post_binary_score_dist', self.reco_type, ['model', 'ym'])

        print('load_score_distribution ---\n')

    def one_shot(self, df_test):
        """
        offline test를 위한 전체 process를 한번에 돌림
        """
        print('one_shot +++')
        
        self.submit_to_item_reco_test(df_test)

        self.insert_feature_importance_top20()

        df_result = self.load_item_reco_test_with_label()
        self.insert_model_stat(df_result)
        self.insert_score_distribution(df_result)
        print('one_shot ---\n')


if __name__ == "__main__":
    # load test data
    print('load_test_data')
    df_test = pd.read_pickle('~/work/upsell/data/df_mart_201904.pkl')
    print('df_test', df_test.shape)

    # load test model
    print('load model')
    test_model = pd.read_pickle('~/work/upsell/model/model_201904.pkl')

    ot4b = OfflineTest4Binary(
        reco_type='upsell',
        test_ym='201904',
        model=test_model,
        model_name='dominic_old_lgbm_04_201907312_test2',
        feature_cols=test_model.feature_name()
    )

    ot4b.one_shot(df_test)
