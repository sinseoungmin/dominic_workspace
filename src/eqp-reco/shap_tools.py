import numpy as np
import pandas as pd
import time
import shap
import random
import math
import matplotlib.pyplot as plt
import seaborn as sns
from eda_tools import numeric_eda_plot

from IPython.display import display
from sklearn.metrics import confusion_matrix
from itertools import chain
import matplotlib as mpl
import joblib

plt.rcParams["font.family"] = 'NanumGothicCoding'
plt.rcParams["font.size"] = 10
mpl.rcParams["axes.unicode_minus"] = False


class ShapModule:
    shap.initjs()

    def __init__(self, X, model, features, idx_col, target_col, label_dict=None, model_type='Python'):
        """
        Args. :
            - X            :   pandas.DataFrame
            - model        :   Model 객체 혹은 str
            - features     :   list 혹은 str
            - idx_col      :   str
              index의 역할을 하는 column이름 (default: svc_mgmt_num)
            - target_col   :   str
              예측하고 싶은 타겟 컬럼 이름
            - model_type   :   python / spark
              사용하는 모델의 종류가 Python모델인지, spark 모델인지의 여부
        """
        assert type(idx_col) == str
        assert type(label_dict) == dict
        self.df = X
        self.df = self.df.drop_duplicates(subset=[idx_col], keep='last')
        self.df.index = self.df[idx_col]
        self.model = model
        self.model_type = model_type
        self.feature_cols = features
        self.idx_col = idx_col
        self.target_col = target_col
        self.explainer = shap.TreeExplainer(model)
        self.label_dict = label_dict
        self.class_label = list(self.label_dict.values())
        print(type(model))

    def calculate_shap_value(self, n_samples=100000, stratified=True, shap_path=None):
        """
        sampling 후 shape value 계산
        Args. :
            - n_samples      :   int
              샘플링 하고 싶은 개수 (default = 100000)
            - stratified     :   bool
              True - label의 비율을 유지한채로 sampling
              False - 단순 random sampling

        """
        assert type(n_samples) == int
        assert type(stratified) == bool
        t1 = time.time()

        if self.target_col is not None:
            # label counts
            label_cnt = self.df.groupby(self.target_col).size().to_dict()
            count = len(self.df)
            sample_ratio = n_samples / count
            # sampling
            if n_samples > 0:
                if not stratified:
                    if n_samples < count:
                        idx = np.random.choice(
                            self.df.index, size=n_samples, replace=False)
                        self.sample_df = self.df.loc[idx]
                    else:
                        self.sample_df = self.df
                else:
                    temp_samples_df = []
                    for k, _ in label_cnt.items():
                        temp_samples_df.append(self.df[self.df[self.target_col] == k].sample(
                            n=int(label_cnt[k] * sample_ratio), random_state=42))
                    self.sample_df = pd.concat(temp_samples_df, axis=0)

            shap_value = self.explainer.shap_values(
                self.sample_df[self.feature_cols])
            if type(self.explainer.expected_value) != list:
                df_sv = pd.DataFrame(shap_value, columns=self.feature_cols, index=self.sample_df.index)
                df_sv = df_sv.join(self.sample_df[self.target_col])
                df_sv_list = []
                df_sv_list.append(df_sv)
            else:
                df_sv_list = list(map(lambda x: pd.DataFrame(shap_value[x], columns=self.feature_cols, index=self.sample_df.index).join(self.sample_df[self.target_col]), range(len(shap_value))))
        else:
            idx = np.random.choice(
                self.df.index, size=n_samples, replace=False)
            self.sample_df = self.df.loc[idx]
            shap_value = self.explainer.shap_values(
                self.sample_df[self.feature_cols])
            if type(self.explainer.expected_value) != list:
                df_sv = pd.DataFrame(
                    shap_value, columns=self.feature_cols, index=self.sample_df.index)
                df_sv_list.append(df_sv)
            else:
                df_sv_list = list(map(lambda x: pd.DataFrame(shap_value[x], columns=self.feature_cols, index=self.sample_df.index), range(len(shap_value))))
        print('elapsed time: ', time.time() - t1)

        self.shap_value = shap_value
        self.sample_df_sv_list = df_sv_list
        self.sample_df_sv = df_sv_list[-1]

    def load_shap_value(self, shap_path):
        self.shap_value = joblib.load(shap_path)
        df_sv_list = list(map(lambda x: pd.DataFrame(self.shap_value[x], columns=self.feature_cols, index=self.df.index), range(len(self.shap_value))))
        self.sample_df_sv_list = df_sv_list
        self.sample_df_sv = df_sv_list[-1]
        self.sample_df = self.df
        print("used saving shap model")

    def _class_labels(self, row_index, class_label, tmp_shap):
        return [f'{class_label[i]} ({self.explainer.expected_value[i] + np.sum(tmp_shap[i][row_index],axis=0).round(2):.2f})' for i in range(len(class_label))]

    def force_plot_custom(self, svc_mgmt_num=None, label=None, orig_df=None):
        """
        단일 고객 force plot - 서번 선택 가능

        Args. :
            - svc_mgmt_num    :   str
              plotting 하고자 하는 서번 넘버
            - label : str or int
              비교하고자 하는 label 명
            - orig_df : Pandas DataFrame
              Numerical embeddding된 Categorical Feature의 원래 값이 저장된 Feature DataFrame (Categorical + Numeric Feature 모두 포함)
        """
        if svc_mgmt_num is None:
            svc_mgmt_num = self.df.iloc[random.randint(
                0, self.df.shape[0])][self.idx_col]
        single_shap_value = self.explainer.shap_values(
            self.df[self.feature_cols].loc[[svc_mgmt_num], :])

        if label is None:
            label_ind = 1
        else:
            class_list = self.model.classes_
            label_ind = np.where(class_list == label)[0][0]
        try:
            pred_score = self.model.predict_proba(self.df.loc[[svc_mgmt_num]][self.feature_cols])[:, label_ind]
        except ValueError:
            pred_score = self.model.predict_proba(self.df.loc[[svc_mgmt_num]][self.feature_cols].values)[:, label_ind]
        except AttributeError:
            print("calculate predict score by your self")
            pred_score = 0.0
        print("=" * 50)
        print(f"model prediction score is : {pred_score} ")
        if self.target_col is not None:
            real_label = self.df.loc[svc_mgmt_num][self.target_col]
            print(f"real target value was [{real_label}]")
        if type(self.explainer.expected_value) == list:
            if orig_df is None:
                return shap.force_plot(self.explainer.expected_value[-1], single_shap_value[-1], self.df[self.feature_cols].loc[svc_mgmt_num], self.feature_cols, out_names=str(svc_mgmt_num))
            else:
                return shap.force_plot(self.explainer.expected_value[-1], single_shap_value[-1], orig_df[self.feature_cols].loc[svc_mgmt_num], self.feature_cols, out_names=str(svc_mgmt_num))
        else:
            return shap.force_plot(self.explainer.expected_value, single_shap_value, self.df[self.feature_cols].loc[svc_mgmt_num], self.feature_cols, out_names=str(svc_mgmt_num))

    def summary_plot_for_confusion_matrix(self, max_display=10, label_false_true=['N', 'Y']):
        """
        confusion matrix별 shap의 summary_plot

        Args. :
            - max_display          :   int
            표출하고 싶은 feature 갯수
            - label_false_true     :   list
            [false를 의미하는 값, true를 의미하는 값]
        """
        sample = self.sample_df
        if len(np.unique(self.model.predict(self.sample_df[self.feature_cols]))) == 2:
            try:
                sample['pred'] = self.model.predict(sample[self.feature_cols])
            except ValueError:
                sample['pred'] = self.model.predict(sample[self.feature_cols].values)
        else:
            pre = self.model.predict(sample[self.feature_cols]) >= 0.5
            sample['pred'] = pre.astype(int)
        fp = list(
            sample.loc[(sample.label == label_false_true[0]) & ((sample.pred == 1) | (sample.pred == 'Y')), :].index)
        tp = list(
            sample.loc[(sample.label == label_false_true[1]) & ((sample.pred == 1) | (sample.pred == 'Y')), :].index)
        tn = list(
            sample.loc[(sample.label == label_false_true[0]) & ((sample.pred == 0) | (sample.pred == 'N')), :].index)
        fn = list(
            sample.loc[(sample.label == label_false_true[1]) & ((sample.pred == 0) | (sample.pred == 'N')), :].index)
        # plotting
        sns.set(rc={'figure.figsize': (20, 5)})

        print('=' * 50)
        print('Confusion matrix')
        confusion_matrix_df = pd.DataFrame([[len(tp), len(fn)], [len(fp), len(tn)]], columns=['Pred:True', 'Pred:False'], index=['Actual:True', 'Actual:False'])
        display(confusion_matrix_df)

        print('=' * 50)
        for row in range(2):
            list_st = row * 2
            list_ed = (row + 1) * 2
            for i, (idx, title) in enumerate(zip([tp, fn, fp, tn][list_st:list_ed], ["TP", "FN", "FP", "TN"][list_st:list_ed])):
                try:
                    plt.subplot(1, 2, (i % 2) + 1)
                    plt.title(title)
                    shap.summary_plot(self.sample_df_sv.loc[idx, self.feature_cols].values, sample.loc[idx, self.feature_cols], max_display=max_display, auto_size_plot=False, show=False)
                except ValueError:
                    plt.subplot(1, 2, (i % 2) + 1)
                    plt.title(title)
            plt.subplots_adjust(wspace=0.3, hspace=0.3)
            plt.show()

    def real_vs_shap_scatter_plot(self, target_feature, distplot=True):
        """
        특정 feature scatter plot(real - shap)

        Args. :
            - target_feature    :  str
              라벨 컬럼 이름
            - Distplot          :  bool
              True일 경우 각 라벨별 분포 표출
        """
        sample = self.sample_df
        sns.set(rc={'figure.figsize': (10, 5)})

        hue = None if self.target_col is None else sample[self.target_col]

        sns.scatterplot(sample[target_feature].values,
                        self.sample_df_sv[target_feature].values, hue=hue)
        plt.title("{}".format(target_feature))
        plt.xlabel("real value")
        plt.ylabel("shap value")
        plt.show()
        if distplot:
            tmp = self.sample_df_sv[[target_feature]]
            tmp.columns = ['[SHAP_VALUE]' + target_feature]
            fig = numeric_eda_plot(sample[[target_feature, self.target_col]].join(tmp), [
                target_feature, '[SHAP_VALUE]' + target_feature], self.target_col, plot_type='density', stat_yn=True)
            return fig

    def real_value_scatter_plot(self, feature1, feature2):
        """
        특정 feature (real - real)

        Args. :
            - feature1    :  str
            - feature2    :  str
        """
        hue = None if self.target_col is None else self.sample_df[self.target_col]
        sns.scatterplot(self.sample_df[feature1].values,
                        self.sample_df[feature2].values, hue=hue)

    def shap_value_scatter_plot(self, feature1, feature2):
        """
        특정 feature (shap - shap)

        Args. :
            - feature1    :  str
            - feature2    :  str
        """
        hue = None if self.target_col is None else self.sample_df[self.target_col]
        sns.scatterplot(
            self.sample_df_sv[feature1].values, self.sample_df_sv[feature2].values, hue=hue)

    # TODO : 한글 컬럼 맵핑하는 거

    def summary_plot(self, max_display=10, show_each_label=False):
        """
        shap summary_plot

        Args. :
            - max_display          :   int
              표출하고 싶은 feature 갯수
            - show_each_label     :    bool
              True일 경우 label별 summary plot 표출
        """
        sample = self.sample_df
        if not show_each_label:
            sns.set(rc={'figure.figsize': (10, 5)})
            shap.summary_plot(
                self.sample_df_sv[self.feature_cols].values, sample[self.feature_cols], max_display=max_display, show=False)
        else:
            sns.set(rc={'figure.figsize': (20, 5)})
            label_list = list(sample[self.target_col].value_counts().index)
            rows = math.ceil(len(label_list) / 2)
            for row in range(rows):
                list_st = row * 2
                list_ed = (row + 1) * 2
                for i, val in enumerate(label_list[list_st:list_ed]):
                    plt.subplot(1, 2, (i % 2) + 1)
                    plt.title(val)
                    shap.summary_plot(self.sample_df_sv[self.sample_df_sv[self.target_col] == val][self.feature_cols].values, sample[sample[self.target_col] == val][self.feature_cols].values, feature_names=self.feature_cols, max_display=max_display, auto_size_plot=False, show=False)
                plt.subplots_adjust(wspace=0.3, hspace=0.3)
                plt.show()

    def dependence_plot(self, target_feature, other_feature='auto'):
        """
        shap dependence_plot 모듈

        Args. :
            - target_feature    :   str
            - other_feature     :   str
        """
        return shap.dependence_plot(target_feature, self.sample_df_sv[self.feature_cols].values, self.sample_df[self.feature_cols], show=False, interaction_index=other_feature)

    def multiclass_decision_plot_custom(self, svc_mgmt_num=None, see_specific=True):
        """
        sampling 후 shape value 계산
        Args. :
            - svc_mgmt_num    :   str
            - class_label     :   list
            - see_specific    :   스코어가 가장 높은 클래스에 대한 상세 정보를 볼 건지에 대한 여부
            - prod_meta       :   prod_meta

        """
        if svc_mgmt_num is None:
            svc_mgmt_num = self.df.iloc[random.randint(
                0, self.df.shape[0])][self.idx_col]
        if self.label_dict is None:
            self.class_label = list(self.df[self.target_col].value_counts().index)
        if str(type(self.model)).startswith("<class 'lightgbm.basic.Booster"):
            tmp_shap = self.explainer.shap_values(self.df.loc[[svc_mgmt_num], self.feature_cols].values)
        else:
            tmp_shap = self.explainer.shap_values(self.df.loc[svc_mgmt_num][self.feature_cols].values)
        if tmp_shap[0].shape[0] > 1:
            cnt = tmp_shap[0].shape[0]
            for x in range(len(self.class_label)):
                tmp_shap[x] = tmp_shap[x].reshape(1, cnt)
        row_index = 0
        # TODO probability 구분
        if self.model_type == 'spark':
            shap.multioutput_decision_plot(list(self.explainer.expected_value), tmp_shap, row_index, feature_names=self.feature_cols, legend_labels=self._class_labels(row_index, self.class_label, tmp_shap), highlight=[np.argmax(self.df.loc[svc_mgmt_num]['probability'])])
        else:
            shap.multioutput_decision_plot(list(self.explainer.expected_value), tmp_shap, row_index, feature_names=self.feature_cols, legend_labels=self._class_labels(row_index, self.class_label, tmp_shap))
        if self.label_dict is not None:
            print("서번: " + str(svc_mgmt_num))
            print("실제 label: " + self.label_dict.get(self.df.loc[svc_mgmt_num][self.target_col]))
        if see_specific is True:
            picked = np.argmax([np.sum(tmp_shap[i][row_index], axis=0) for i in range(len(self.class_label))])
            shap.decision_plot(self.explainer.expected_value[picked], tmp_shap[picked][0], self.df.loc[svc_mgmt_num][self.feature_cols], feature_display_range=slice(None, -31, -1))

    def multiclass_summary_plot(self, which_class=None):
        if which_class is None:
            for which_class, class_nm in enumerate(self.class_label):
                shap.summary_plot(self.sample_df_sv_list[which_class][self.feature_cols].values, self.sample_df[self.feature_cols], max_display=10, auto_size_plot=False, show=False)
                plt.title(class_nm)
                plt.show()
        else:
            shap.summary_plot(self.sample_df_sv_list[which_class][self.feature_cols].values, self.sample_df[self.feature_cols], max_display=10, auto_size_plot=False, show=False)
            plt.title(self.class_label[which_class])

    def multiclass_force_plot(self, which_class=None, svc_mgmt_num=None):
        if svc_mgmt_num is None:
            svc_mgmt_num = self.sample_df.iloc[random.randint(
                0, self.sample_df.shape[0])][self.idx_col]
        if str(type(self.model)).startswith("<class 'lightgbm.basic.Booster"):
            tmp_shap = self.explainer.shap_values(self.df.loc[[svc_mgmt_num], self.feature_cols].values)
        else:
            tmp_shap = self.explainer.shap_values(self.df.loc[svc_mgmt_num][self.feature_cols].values)
        if tmp_shap[0].shape[0] > 1:
            cnt = tmp_shap[0].shape[0]
            for x in range(len(self.class_label)):
                tmp_shap[x] = tmp_shap[x].reshape(1, cnt)
        if which_class is None:
            for which_class, class_nm in enumerate(self.class_label):
                print(class_nm)
                display(shap.force_plot(self.explainer.expected_value[which_class], tmp_shap[which_class], self.df.loc[svc_mgmt_num][self.feature_cols], self.feature_cols, out_names=str(svc_mgmt_num)))
        else:
            print(self.class_label[which_class])
            if self.target_col is not None:
                real_label = self.label_dict[self.df.loc[svc_mgmt_num][self.target_col]]
            print(f"real target value was [{real_label}]")
            return shap.force_plot(self.explainer.expected_value[which_class], tmp_shap[which_class], self.df.loc[svc_mgmt_num][self.feature_cols], self.feature_cols, out_names=str(svc_mgmt_num))

    def multiclass_dependence_plot(self, which_class=None, target_feature='bas_fee_amt', other_feature='auto'):
        if which_class is not None:
            display(shap.dependence_plot(target_feature, self.sample_df_sv_list[which_class][self.feature_cols].values, self.sample_df[self.feature_cols], show=True, interaction_index=other_feature, title=self.class_label[which_class]))
        elif which_class is None:
            for which_class, class_nm in enumerate(self.class_label):
                display(shap.dependence_plot(target_feature, self.sample_df_sv_list[which_class][self.feature_cols].values, self.sample_df[self.feature_cols], show=True, interaction_index=other_feature, title=class_nm))

    def multiclass_summary_plot_for_confusion_matrix(self, y_pred=None):
        class_label = list(self.label_dict.values())
        if self.model_type == 'spark':
            y_pred = list(self.sample_df.prediction)
            y_true = list(self.sample_df.indexedLabel)
        elif self.model_type == 'python':
            y_true = list(self.sample_df[self.target_col])
            if y_pred is None:
                print("please pass y_pred argument")
            else:
                self.sample_df['prediction'] = y_pred
            self.sample_df['indexedLabel'] = y_true
        else:
            print("model type is not readable")
        result_matrix = confusion_matrix(y_true, y_pred)
        result_matrix = pd.DataFrame(result_matrix, columns=class_label)
        result_matrix.index = list(map(lambda x: str(x) + '_true', class_label))
        result_matrix.columns = list(map(lambda x: str(x) + '_pred', class_label))
        w, h = len(self.explainer.expected_value), len(self.explainer.expected_value)
        result_idx = [[0 for x in range(w)] for y in range(h)]
        if self.model_type == 'python':
            for i, i_key in enumerate(self.label_dict.keys()):
                for j, j_key in enumerate(self.label_dict.keys()):
                    result_idx[j][i] = list(self.sample_df.loc[(self.sample_df.prediction == i_key) & (self.sample_df.indexedLabel == j_key)].index)
        elif self.model_type == 'spark':
            for i, i_key in enumerate(self.label_dict.keys()):
                for j, j_key in enumerate(self.label_dict.keys()):
                    result_idx[j][i] = list(self.sample_df.loc[(self.sample_df.prediction == i) & (self.sample_df.indexedLabel == j)].index)
        self.confusion_idx = result_idx
        self.confusion_matrix = result_matrix
        display(result_matrix)
        for which_class, label in enumerate(class_label):
            except_which = list(set(list(range(w))) - set(list([which_class])))
            tp = list(result_idx[which_class][which_class])
            fn = list(chain(*np.array(result_idx[which_class])[except_which]))
            fp = list(chain(*np.array(result_idx)[:, 0][except_which]))
            tn = set(self.sample_df.index) - set(tp) - set(fn) - set(fp)
            print('=' * 50)
            print('Confusion matrix')
            sns.set(rc={'figure.figsize': (20, 5)})
            confusion_matrix_df = pd.DataFrame([[len(tp), len(fn)], [len(fp), len(tn)]], columns=['Pred:True', 'Pred:False'], index=['Actual:True', 'Actual:False'])
            display(confusion_matrix_df)

            print('=' * 50)
            print(label)
            for row in range(2):
                list_st = row * 2
                list_ed = (row + 1) * 2
                for i, (idx, title) in enumerate(zip([tp, fn, fp, tn][list_st:list_ed], ["TP", "FN", "FP", "TN"][list_st:list_ed])):
                    try:
                        plt.subplot(1, 2, (i % 2) + 1)
                        plt.title(title)
                        shap.summary_plot(self.sample_df_sv_list[which_class].loc[idx, self.feature_cols].values, self.sample_df.loc[idx, self.feature_cols], max_display=10, auto_size_plot=False, show=False)
                    except ValueError:
                        plt.subplot(1, 2, (i % 2) + 1)
                        plt.title(title)
                    except KeyError:
                        print(title)
                    except IndexError:
                        print("there is no ")
                plt.subplots_adjust(wspace=0.3, hspace=0.3)
                plt.show()
        print("confusion_idx is created")

    def multiclass_summary_selected(self, true_class, pred_class, y_pred=None):
        if self.model_type == 'spark':
            w, h = len(self.explainer.expected_value), len(self.explainer.expected_value)
            result_idx = [[0 for x in range(w)] for y in range(h)]
            for i in range(w):
                for j in range(w):
                    result_idx[j][i] = list(self.sample_df.loc[(self.sample_df.prediction == i) & (self.sample_df.indexedLabel == j)].index)
            self.confusion_idx = result_idx
        elif self.model_type == 'python':
            if y_pred is None:
                print("you should put y_pred value")
            else:
                self.sample_df['prediction'] = y_pred
                self.sample_df['indexedLabel'] = list(self.sample_df[self.target_col])
                w, h = len(self.explainer.expected_value), len(self.explainer.expected_value)
                result_idx = [[0 for x in range(w)] for y in range(h)]
                for i, i_key in enumerate(self.label_dict.keys()):
                    for j, j_key in enumerate(self.label_dict.keys()):
                        result_idx[j][i] = list(self.sample_df.loc[(self.sample_df.prediction == i_key) & (self.sample_df.indexedLabel == j_key)].index)
                self.confusion_idx = result_idx
        selected_idx = list(self.confusion_idx[true_class][pred_class])
        try:
            shap.summary_plot(self.sample_df_sv_list[pred_class][self.feature_cols].loc[selected_idx].values, self.sample_df[self.feature_cols].loc[selected_idx], self.feature_cols)
        except ValueError:
            print("there is no svc_mgmt_num")
