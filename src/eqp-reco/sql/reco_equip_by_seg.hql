create table dumbo.reco_equip_by_seg as 
select  a.svc_mgmt_num -- 서비스 관리번호
,       a.eqp_mdl_cd -- 단말코드
,       a.age -- 나이
,       a.prod_nm -- 요금제 명
,       b.rep_eqp_mdl_cd
,       b.eqp_nm_rmk
,       b.eqp_pet_nm
,       b.mfact_nm
,       b.mktg_dt
,       b.first_eqp_out_prc
,       b.out_prc
,       b.ym
from (
    select  svc_mgmt_num
    ,       eqp_mdl_cd
    ,       age
    ,       prod_nm
    ,       concat(substr(add_months(from_unixtime(unix_timestamp(ym,'yyyyMM'), 'yyyy-MM-dd'), 1), 1,4),  
                    substr(add_months(from_unixtime(unix_timestamp(ym,'yyyyMM'), 'yyyy-MM-dd'), 1), 6,2)) as next_ym
    from comm.user_profile_pivot_monthly
    where ym between '201902' and '201907'
) a
JOIN (
    select svc_mgmt_num
    ,       rep_eqp_mdl_cd -- 대표단말코드
    ,       eqp_nm_rmk -- 단말기명?
    ,       eqp_pet_nm -- 단말기팻명(대표단말코드 단위)
    ,       mfact_nm -- 제조사명
    ,       mktg_dt -- 출시일자
    ,       first_eqp_out_prc -- 초기 출고가
    ,       out_prc
    ,       ym
    from dumbo.user_label_matrix_for_reco_equip_monthly
) b 
on a.svc_mgmt_num = b.svc_mgmt_num and a.next_ym = b.ym
;