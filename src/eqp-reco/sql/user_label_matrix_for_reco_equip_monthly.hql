create table dumbo.user_label_matrix_for_reco_equip_monthly as
SELECT  a.svc_mgmt_num -- 서비스관리번호
,       a.chg_dt -- 변경날짜
,       a.eqp_mdl_cd -- 단말코드
,       a.sale_chnl_cl_cd -- 기변채널
,       b.rep_eqp_mdl_cd -- 대표단말코드
,       b.eqp_nm_rmk -- 단말기명?
,       b.eqp_pet_nm -- 단말기팻명(대표단말코드 단위)
,       b.mfact_nm -- 제조사명
,       b.mktg_dt -- 출시일자
-- ,       b.smart_phon_yn -- 스마트폰 여부
,       b.tablet_yn -- 태블릿 여부
,       b.note_yn -- 노트 여부
,       b.disp_size_larg_yn -- 대화면 여부
,       b.eqp_siz_ctt -- 화면 사이즈
,       b.flagship_yn -- flagsip대상 여부
,       b.eqp_mdl_size -- 단말기크기(용량)
,       b.eqp_mdl_ntwk -- 단말기 네트워크 정보(4g,5g)
,       b.first_eqp_out_prc -- 초기 출고가
,       cast(c.out_prc as int) as out_prc -- 당월 출고가
,       a.ym
FROM
(
    SELECT  svc_mgmt_num
    ,       chg_dt
    ,       eqp_mdl_cd
    ,       sale_chnl_cl_cd -- channel 코드
    ,       ym
    FROM    comm.mmap_svc_eqp_chg_f -- 기변 이력
    WHERE   svc_chg_rsn_cd in ('C145', 'C245') -- 판매기변 
    AND     ym between '201903' and '201908'
) a
LEFT JOIN    dumbo.aips_equip_meta b -- 단말기 메타 테이블
ON      a.eqp_mdl_cd = b.eqp_mdl_cd
LEFT JOIN    comm.td_zeqp_eqp_out_amt c -- 단말기 출고가 테이블
ON      a.eqp_mdl_cd = c.eqp_mdl_cd
AND     a.ym = c.aply_ym
;
e