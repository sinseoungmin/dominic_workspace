create table dumbo.aips_equip_meta as
select
        a.eqp_mdl_cd -- 단말코드
,       b.rep_eqp_mdl_cd -- 대표단말코드
,       b.rmk as eqp_nm_rmk -- 단말기명?
,       a.eqp_pet_nm -- 단말기팻명(대표단말코드 단위)
,       a.mfact_nm -- 제조사명
,       a.mktg_dt -- 출시일자
,       a.smart_phon_yn -- 스마트폰 여부
,       a.tablet_yn -- 태블릿 여부
,       a.note_yn -- 노트 여부
,       a.disp_size_larg_yn -- 대화면 여부
,       b.eqp_siz_ctt -- 화면 사이즈
,       a.flagship_yn -- flagsip대상 여부
,       a.eqp_mdl_size -- 단말기크기(용량)
,       a.eqp_mdl_ntwk -- 단말기 네트워크 정보(4g,5g)
,       cast(c.out_prc as int) as first_eqp_out_prc -- 초기 출고가
from saturn.mcom_eqp_mdl a
left join comm.td_zeqp_eqp_mdl b on a.eqp_mdl_cd = b.eqp_mdl_cd
left join (
    select  eqp_mdl_cd, out_prc
    from (
        select  eqp_mdl_cd, out_prc
        ,       row_number() over(partition by eqp_mdl_cd order by aply_ym) as rn
        from comm.td_zeqp_eqp_out_amt
    ) a
    where a.rn = 1
) c on a.eqp_mdl_cd = c.eqp_mdl_cd
where a.dt = '20190825'
and   a.eqp_pet_nm not like '%TEST%'
and   a.eqp_pet_nm not like '%(KTF)%'
and   a.eqp_pet_nm not like '%(LGU)%'
and   a.eqp_pet_nm not like '%(MVNO)%'
and   a.eqp_pet_nm not like '%개인인증%'
and   a.eqp_pet_nm not like '%매장용%'
and   a.eqp_pet_nm not like '%DEMO%'
and   a.mfact_nm not like '%TEST%'
and   a.mfact_nm not like '%타사%'
and   a.mfact_nm not like '%블랙리스트%'
and   a.mfact_nm != 'MVNO'
and   a.mktg_dt > '20101231'
and   b.rmk not like '%DEMO%'
and   b.rmk != '미정'
and   (a.smart_phon_yn='Y' or a.tablet_yn='Y')
;