INSERT OVERWRITE TABLE dumbo.user_profile_equip_info partition (ym)
select  a.svc_mgmt_num -- 서비스 관리번호
,       a.eqp_acqr_dt -- 단말기 획득일자
,       a.eqp_mdl_cd -- 단말코드
,       a.eqp_out_prc -- 단말기 출고가
,       a.op_sale_chnl_cl_cd -- 단말기 기변 채널
,       a.old_eqp_yn -- 단말기 중고여부
,       b.rep_eqp_mdl_cd -- 대표단말코드
,       b.eqp_nm_rmk -- 단말기명?
,       b.eqp_pet_nm -- 단말기팻명(대표단말코드 단위)
,       b.mfact_nm -- 제조사명
,       b.mktg_dt -- 출시일자
,       b.smart_phon_yn -- 스마트폰 여부
,       b.tablet_yn -- 태블릿 여부
,       b.note_yn -- 노트 여부
,       b.disp_size_larg_yn -- 대화면 여부
,       b.eqp_siz_ctt -- 화면 사이즈
,       b.flagship_yn -- flagsip대상 여부
,       b.eqp_mdl_size -- 단말기크기(용량)
,       b.eqp_mdl_ntwk -- 단말기 네트워크 정보(4g,5g)
,       b.first_eqp_out_prc -- 초기 출고가
,       c.eqp_mdl_cd as prev_eqp_mdl_cd -- 이전에 사용한 단말기 코드
,       d.eqp_pet_nm as prev_eqp_pet_nm -- 이전에 사용한 단말기 펫네임
,       d.mfact_nm as prev_mfact_nm  -- 이전에 사용한 단말기 제조사
,       d.first_eqp_out_prc as prev_first_eqp_out_prc -- 이전에 사용한 단말기 초기 출고가
,       a.ym
from (
    select  svc_mgmt_num
    ,       eqp_mdl_cd
    ,       op_sale_chnl_cl_cd
    ,       eqp_acqr_dt
    ,       old_eqp_yn
    ,       eqp_out_prc
    ,       ym2
    from comm.mmkt_svc_bas_f
    where ym = '201904'
    and svc_mgmt_num in (
        select distinct svc_mgmt_num
        from dumbo.user_label_matrix_for_reco_equip_monthly
        where ym = '201905'
    )
) a
JOIN    dumbo.aips_equip_meta b -- 단말기 메타 테이블
ON      a.eqp_mdl_cd = b.eqp_mdl_cd
left join (
    select svc_mgmt_num, eqp_mdl_cd
    from (
        select svc_mgmt_num, eqp_mdl_cd,row_number() over(partition by svc_mgmt_num order by ym desc) as rn
        from comm.mmap_svc_eqp_chg_f
        where ym <= '201904'
    ) a
    where rn = 2
) c
on a.svc_mgmt_num = c.svc_mgmt_num
LEFT JOIN    dumbo.aips_equip_meta d -- 단말기 메타 테이블
ON      c.eqp_mdl_cd = d.eqp_mdl_cd
;