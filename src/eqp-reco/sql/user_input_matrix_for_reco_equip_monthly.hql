create table dumbo.user_input_matrix_for_reco_equip_monthly as
select b.*
,       a.eqp_acqr_dt -- 단말기 획득일자
,       a.eqp_mdl_cd -- 단말코드
,       a.eqp_out_prc -- 단말기 출고가
,       a.op_sale_chnl_cl_cd -- 단말기 기변 채널
,       a.old_eqp_yn -- 단말기 중고여부
,       a.rep_eqp_mdl_cd -- 대표단말코드
,       a.eqp_nm_rmk -- 단말기명?
,       a.eqp_pet_nm -- 단말기팻명(대표단말코드 단위)
,       a.mfact_nm -- 제조사명
,       a.mktg_dt -- 출시일자
,       a.smart_phon_yn -- 스마트폰 여부
,       a.tablet_yn -- 태블릿 여부
,       a.note_yn -- 노트 여부
,       a.disp_size_larg_yn -- 대화면 여부
,       a.eqp_siz_ctt -- 화면 사이즈
,       a.flagship_yn -- flagsip대상 여부
,       a.eqp_mdl_size -- 단말기크기(용량)
,       a.eqp_mdl_ntwk -- 단말기 네트워크 정보(4g,5g)
,       a.first_eqp_out_prc -- 초기 출고가
,       a.prev_eqp_mdl_cd -- 이전에 사용한 단말기 코드
,       a.prev_eqp_pet_nm -- 이전에 사용한 단말기 펫네임
,       a.prev_mfact_nm  -- 이전에 사용한 단말기 제조사
,       a.prev_first_eqp_out_prc -- 이전에 사용한 단말기 초기 출고가
from dumbo.user_profile_equip_info a
left join comm.user_profile_pivot_monthly b
on a.svc_mgmt_num = b.svc_mgmt_num 
and a.ym = b.ym