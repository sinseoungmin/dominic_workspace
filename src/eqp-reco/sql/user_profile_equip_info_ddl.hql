CREATE EXTERNAL TABLE dumbo.user_profile_equip_info (
        svc_mgmt_num string -- 서비스 관리번호
,       eqp_acqr_dt string -- 단말기 획득일자
,       eqp_mdl_cd string -- 단말코드
,       eqp_out_prc bigint -- 단말기 출고가
,       op_sale_chnl_cl_cd string -- 단말기 기변 채널
,       old_eqp_yn string -- 단말기 중고여부
,       rep_eqp_mdl_cd string -- 대표단말코드
,       eqp_nm_rmk string -- 단말기명?
,       eqp_pet_nm string -- 단말기팻명(대표단말코드 단위)
,       mfact_nm string -- 제조사명
,       mktg_dt string -- 출시일자
,       smart_phon_yn string -- 스마트폰 여부
,       tablet_yn string -- 태블릿 여부
,       note_yn string -- 노트 여부
,       disp_size_larg_yn string -- 대화면 여부
,       eqp_siz_ctt string -- 화면 사이즈
,       flagship_yn string -- flagsip대상 여부
,       eqp_mdl_size string -- 단말기크기(용량)
,       eqp_mdl_ntwk string -- 단말기 네트워크 정보(4g,5g)
,       first_eqp_out_prc bigint -- 초기 출고가
,       prev_eqp_mdl_cd string -- 이전에 사용한 단말기 코드
,       prev_eqp_pet_nm string -- 이전에 사용한 단말기 펫네임
,       prev_mfact_nm  string -- 이전에 사용한 단말기 제조사
,       prev_first_eqp_out_prc bigint -- 이전에 사용한 단말기 초기 출고가
) PARTITIONED BY (ym string) STORED AS PARQUET
LOCATION 'hdfs://yellowelephant/data/dumbo/user_profile_equip_info'

